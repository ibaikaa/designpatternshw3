//
//  main.swift
//  designPatterns
//
//  Created by ibaikaa on 22/3/23.
//

import Foundation

// MARK: - Factory method pattern usage. FILE: Factory.swift
print("--- Factory pattern ---")

// MARK: – Factory. 1 пример.
print("1 пример: -----")

// Создаем объект класса Level уже через фабрику
let levelTwo = LevelFactory.createLevel(levelNumber: 2)

// Теперь, можно и через цикл создать массив из уровней:
var levels = [Level]()
for i in 1...3 {
    levels.append(LevelFactory.createLevel(levelNumber: i))
}
levels.forEach { print($0.name) }

// На выходе получим:
//  Level 1
//  Level 2
//  Level 3

// MARK: – Factory. 2 пример.
print("2 пример: -----")

//Теперь, создание объектов Document происходит через фабрику, и это безопаснее и легче для модификации и расширения.
let docs: [Document] = [
   DocumentFactory.createDocument(type: .text, name: "Дипломная работа", size: 512),
   DocumentFactory.createDocument(type: .graphical, name: "SVG иконка проекта", size: 1230)
]
docs.forEach { $0.open() }
// Output:
//  Текстовый документ "Дипломная работа" с размером 512.0 КБ открыт.
//  Графический документ "SVG иконка проекта" с размером 1230.0 КБ открыт.

print("")

// MARK: - Builder method pattern usage. FILE: Builder.swift
print("--- Builder pattern ---")

// MARK: – Builder. 1 пример.
print("1 пример: -----")

// Теперь, удобно создавать "кастомные" пиццы со своей настройкой параметров, несмотря на то что изначально класс Пицца является сложным с кучей параметров
//Создадим сложную пеперонни, задействовав все методы билдера
print("\nПервая (сложная) пицца:")
let peperroni = PizzaBuilder(name: "Пепперонни")
   .setIngredients(ingredients: "Цыпленок, соус альфркедо, соленые огурчики, томаты")
   .setCheese(cheese: "Моцарелла")
   .setSize(size: .large)
   .setAdditions(additions: ["Цыпленок, Острый халапеньо"])
   .setCheseSide(included: true)
   .setDoughThickness(.thin)
   .build()

print(peperroni.description)

// На выходе получаем:
//  Название: Пепперонни;
//  Состав: Цыпленок, соус альфркедо, соленые огурчики, томаты;
//  Сыр: Моцарелла;
//  Размер пиццы: Большая;
//  Добавки: Цыпленок, Острый халапеньо;
//  Сырный бортик включен;
//  Тесто пиццы: Тонкое.

//Создадим простую сырную пиццу с сыром Король Артур
print("\nВторая (простая) пицца:")
let simpleCheesePizza = PizzaBuilder(name: "Сырная пицца")
   .setCheese(cheese: "Король Артур")
   .build()
print(simpleCheesePizza.description)

// На выходе получаем:
//  Название: Сырная пицца;
//  Состав: Нет данных;
//  Сыр: Король Артур;
//  Размер пиццы: Средняя;
//  Тесто пиццы: Традиционное.

// MARK: – Builder. 2 пример.
print("2 пример: -----")

// Теперь, если не указывать ни один из параметров, а просто "строить" дом через HouseBuilder, у нас будет дефолтный дом со значением 0 и false для булевых параметров:

let simpleHouse = HouseBuilder().build()
print(simpleHouse.description)
// Output:
//Это дом с 0 спальнями, 0 ванными комнтами с 0 комнатами в целом и в нем 0 окон. Площадь дома: 0.0 кв.м.

// И, теперь удобнее создавать и сложный дом с кучей параметров:
let complexHouse = HouseBuilder()
    .withWindows(4)
    .withDoors(5)
    .withRooms(10)
    .withNumberOfBedrooms(4)
    .withNumberOfBathrooms(3)
    .withSquareFootage(350)
    .withPool(true)
    .withGarage(true)
    .withSmartHome(true)
    .build()

print(complexHouse.description)
// Output:
//  Это дом с 4 спальнями, 3 ванными комнтами с 10 комнатами в целом и в нем 4 окон. Площадь дома: 350.0 кв.м. Также, у него есть бассейн. Также, есть собственный гараж на участке дома. И самое замечательное, здесь подключена технология "Smart House" (Умный Дом).

print("")

// MARK: - Facade method pattern usage. FILE: Facade.swift
print("--- Facade pattern ---")

// MARK: – Facade. 1 пример.
print("1 пример: -----")

let car = Car()
car.startCar()

// Output:
//  Мотор начал свою работу.
//  GPS настроен на текущее местоположение.
//  Машина заработала

car.stopCar()

// Output:
//  Мотор заглушился.
//  GPS перестал работать.
//  Машина перестала работать

// MARK: – Facade. 2 пример.
print("2 пример: -----")

let lesson = GeeksLesson()
lesson.startLesson()

// Output:
//  Модем был включен. Wifi в аудитории работает.
//  HDMI подключен к проектору.
//  Проектор был включен. Картинка работает хорошо.
//  Ноутбук преподавателя был подключен к проектору. Все работает как нужно.
//  Конференция ЗУМ для урока была создана преподавателем и отправлена в группу в телеграме.
//  Урок начался.

lesson.stopLesson()

// Output:
//  Модем был выключен. Wifi в аудитории не работает.
//  HDMI отключен от проектора.
//  Проектор был выключен. Пусть остывает.
//  Ноутбук преподавателя был отключен от проектора.
//  Конференция ЗУМ для урока была окончена.
//  Урок окончен.

print("")

// MARK: - Strategy method pattern usage. FILE: Strategy.swift
print("--- Strategy pattern --- ")

// MARK: – Strategy. 1 пример.
print("1 пример: -----")

let navigator = Navigator()
navigator.makeRoute(from: "Elebaeva str", to: "Ibraimova str")
navigator.setRouteStrategy(.walk)
navigator.makeRoute(from: "Ibraimova str", to: "ЦУМ")
navigator.setRouteStrategy(.publicTransport)
navigator.makeRoute(from: "ЦУМ", to: "Филармония")

// Output:
//  Построен маршрут для путешествий на автомобиле от "Elebaeva str" до "Ibraimova str".
//  Построен маршрут для пеших путешествий от "Ibraimova str" до "ЦУМ".
//  Построен маршрут для путешествий на общественном транспорте от "ЦУМ" до "Филармония".

// MARK: – Strategy. 2 пример.
print("2 пример: -----")

let keyboard = Keyboard()
keyboard.printInputSourceInfo()
keyboard.switchInputSource(to: .russian)
keyboard.printInputSourceInfo()
keyboard.switchInputSource(to: .french)
keyboard.printInputSourceInfo()

// Output:
//  Input source in English. Everything will be printed in English.
//  Источник ввода на русском языке. Все будет печататься на русском языке.
//  Источник ввода на французском языке. Все будет печататься на французском языке.



