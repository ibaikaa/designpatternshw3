//
//  Strategy.swift
//  designPatterns
//
//  Created by ibaikaa on 22/3/23.
//

import Foundation

// Стратегия – поведенческий паттерн проектирования, который определяет семейство схожих алгоритмов и помещает каждый из них в собственный класс, после чего алгоритмы можно взаимозаменять прямо во время исполнения программы.

// MARK: - 1 пример.
protocol NavigatorStrategy {
    func makeRoute(from a:String, to b: String)
}

class RoadStrategy: NavigatorStrategy {
    func makeRoute(from a: String, to b: String) {
        print("Построен маршрут для путешествий на автомобиле от \"\(a)\" до \"\(b)\".")
    }
}

class WalkStrategy: NavigatorStrategy {
    func makeRoute(from a: String, to b: String) {
        print("Построен маршрут для пеших путешествий от \"\(a)\" до \"\(b)\".")
    }
}

class PublicTransportStrategy: NavigatorStrategy {
    func makeRoute(from a: String, to b: String) {
        print("Построен маршрут для путешествий на общественном транспорте от \"\(a)\" до \"\(b)\".")
    }
}

class Navigator {
    enum StrategyType {
        case road, walk, publicTransport
    }
    private var strategy: NavigatorStrategy = RoadStrategy()
    
    func setRouteStrategy(_ routeStrategy: StrategyType) {
        switch routeStrategy {
        case .road: strategy = RoadStrategy()
        case .walk: strategy = WalkStrategy()
        case .publicTransport: strategy = PublicTransportStrategy()
        }
    }
    
    func makeRoute(from a: String, to b: String) {
        strategy.makeRoute(from: a, to: b)
    }
}

// MARK: - 2 пример.

// Пример: Источники ввода клавиатуры.
// Стратегия решает проблему нагруженного класса с кучей сложных методов и параметров, разбивая все сложные методы на общий интерфейс и классы, каждый из которых служит для реализации алгоритма по своему.

protocol InputSourceStrategy {
    func printInputSourceInfo()
    
}

class RussianLanguageInputSourceStrategy: InputSourceStrategy {
    func printInputSourceInfo() {
        print("Источник ввода на русском языке. Все будет печататься на русском языке.")
    }
}

class EnglishLanguageInputSourceStrategy: InputSourceStrategy {
    func printInputSourceInfo() {
        print("Input source in English. Everything will be printed in English.")
    }
}

class FrenchLanguageInputSourceStrategy: InputSourceStrategy {
    func printInputSourceInfo() {
        print("Источник ввода на французском языке. Все будет печататься на французском языке.")
    }
}

class Keyboard {
    enum InputSourceType {
        case russian, english, french
    }
    
    private var strategy: InputSourceStrategy = EnglishLanguageInputSourceStrategy()
    
    func switchInputSource(to type: InputSourceType) {
        switch type {
        case .russian:
            self.strategy = RussianLanguageInputSourceStrategy()
        case .english:
            self.strategy = EnglishLanguageInputSourceStrategy()
        case .french:
            self.strategy = FrenchLanguageInputSourceStrategy()
        }
    }
    
    func printInputSourceInfo() {
        strategy.printInputSourceInfo()
    }
}

