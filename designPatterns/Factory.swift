//
//  Factory.swift
//  designPatterns
//
//  Created by ibaikaa on 22/3/23.
//

import Foundation

// Фабричный метод – порождающий паттерн, упрощающий создание объектов класса благодаря созданию общего класса, который может создавать эти самые объекты внутри своего метода

// MARK: - 1 пример.

// Представим,что у нас есть простая программа, имитирующая видеоигру, в которой игроки могут выбирать и играть на разных уровнях. Каждый уровень имеет свой уникальный набор врагов и препятствий.

class Level {
    var name: String
    var enemies: [String]
    var obstacles: [String]
    
    init(name: String, enemies: [String], obstacles: [String]) {
        self.name = name
        self.enemies = enemies
        self.obstacles = obstacles
    }
    
}

// Создание levelOne, объекта класса Level
let levelOne = Level(name: "Level 1", enemies: ["Enemy 1", "Enemy 2"], obstacles: ["Obstacle 1", "Obstacle 2"])

// Проблема: Если у нас будет 100 уровней, то нам придется вручную прописывать настройки (свойства enemies, obstacles) для ста уровней.

// Как фабричный метод может решить эту проблему?

// Создадим класс LevelFactory, в котором есть static метод createLevel(), принимающий в себя параметр – номер уровня.
// Через switch case определяем значение levelNumber и исходя из кейса, возвращаем соответсвующий уровень
class LevelFactory {
    
    static func createLevel(levelNumber: Int) -> Level {
        switch levelNumber {
        case 1:
            return Level(
                name: "Level 1",
                enemies: ["Enemy 1", "Enemy 2"],
                obstacles: ["Obstacle 1", "Obstacle 2"]
            )
        case 2:
            return Level(
                name: "Level 2",
                enemies: ["Enemy 3", "Enemy 4"],
                obstacles: ["Obstacle 3", "Obstacle 4"]
            )
        case 3:
            return Level(
                name:"Level 3",
                enemies: ["Enemy 5", "Enemy 6"],
                obstacles: ["Obstacle 5", "Obstacle 6"]
            )
        default:
            return Level(name: "Default Level", enemies: [], obstacles: [])
        }
    }
    
}

// MARK: - 2 пример.

// Представим, что мы пишем приложение для работы с документами разного типа: пока что только текстового и графического.
// Создадим общий протокол Document, который в себе носит свойства и метод open(), которые должны быть у наших будущих классов: TextDocument и GraphicalDocument

protocol Document {
    var name: String { get }
    var size: Double { get }
    
    func open ()
}

// Создадим два класса: TextDocument и GraphicalDocument, подписанные на протокол Document
class TextDocument: Document {
    var name: String
    var size: Double
    
    init(name: String, size: Double) {
        self.name = name
        self.size = size
    }
    
    func open() {
        print("Текстовый документ \"\(name)\" с размером \(size) КБ открыт.")
    }
}

class GraphicalDocument: Document {
    var name: String
    var size: Double
    
    init(name: String, size: Double) {
        self.name = name
        self.size = size
    }
    
    func open() {
        print("Графический документ \"\(name)\" с размером \(size) КБ открыт.")
    }
}

// Упростим создание множества документов через фабрику:
// Создадим перечисление возможных типов документа. Почему перечисление, а не String?
// Потому что это легче расширять и безопаснее.
enum DocumentType {
    case text, graphical
}

// Создадим класс DocumentFactory, со static методом createDocument() который будет возвращать TextDocument/GraphicalDocument, в зависимости от типа документа (парам. type: DocumentType)
class DocumentFactory {
    static func createDocument(type: DocumentType, name: String, size: Double) -> Document {
        switch type {
        case .text: return TextDocument(name: name, size: size)
        case .graphical: return GraphicalDocument(name: name, size: size)
        }
    }
    
}




