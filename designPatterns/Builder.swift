//
//  Builder.swift
//  designPatterns
//
//  Created by ibaikaa on 22/3/23.
//

import Foundation

// Строитель – порождающий паттерн, позволяющий решать проблему создания сложных объектов, которые и еще должны быть по разному представлены

// MARK: - 1 пример.

// Представим, что мы пишем приложение для пиццерии, у которой, логично, основной продукт – пицца.
// Но пицца является сложным объектом и по желанию клиентов может быть с разными начинками, а то и вовсе, без начинок.
// Если просто создать объект пиццы, и передавать в параметр начинки пустоту, то код становится сложнее для восприятия и поддержки.
// Вместо этого можно использовать паттерн Строитель для создания объекта пиццы.

// Создадим класс Пицца с перечислениями для размера пиццы и толщины теста
enum PizzaSize: String {
    case little = "Маленькая", medium = "Средняя", large = "Большая"
}

enum DoughThickness: String {
    case common = "Традиционное", thin = "Тонкое"
}

class Pizza {
    var name: String
    var ingredients, cheese: String?
    var size: PizzaSize = .medium
    var addditions: [String] = [] // добавки
    var cheeseSide: Bool = false // сырный бортик
    var dough: DoughThickness = .common //тесто пиццы
    
    init(name: String) {
        self.name = name
    }
    
    // Пропишем метод description
    public var description: String {
        var description = "Название: \(name);\n"
        description += "Состав: \(ingredients.isNilOrEmpty ? "Нет данных" : ingredients!);\n"
        if !cheese.isNilOrEmpty { description += "Сыр: \(cheese!);\n" }
        description += "Размер пиццы: \(size.rawValue);\n"
        if !addditions.isEmpty { description += "Добавки: \(addditions.joined(separator:" "));\n"}
        if cheeseSide { description += "Сырный бортик включен;\n"}
        description += "Тесто пиццы: \(dough.rawValue)."
        return description
    }
    
}

// Практика isNilOrEmpty для удобства с работой с опциональным типами данных для коллекций. Используется для вычисляемого свойства description класса Pizza
extension Optional where Wrapped: Collection {
    var isNilOrEmpty: Bool {
           return self?.isEmpty ?? true
    }
}

// Как мы видим, класс Pizza является сложным, имея в себе уйму параметров (еще не все, что можно добавить), хорошо, мы прописали значения по умолчанию, которые не надо теперь прописывать в методе init(), что делает инициализатор чуточку проще.
// Но, вдруг, нам нужна пицца, в котоой нам нужны добавки, сырный бортик, указать размер пиццы на маленький, тесто поменять на тонкое. Нам придется вызывать каждое из свойств после создания объекта, чтоб менять им значения. Это неудобно и непрактично.
// Вместо этого, можно создать класс-строитель, который будет вызываться для создания определенной пиццы по мере нужных параметров.

// Создадим класс PizzaBuilder
class PizzaBuilder {
    private var pizza: Pizza
    
    init(name: String) {
        pizza = Pizza(name: name)
    }
    
    func setIngredients(ingredients: String) -> PizzaBuilder {
        pizza.ingredients = ingredients
        return self
    }
    
    func setCheese(cheese: String) -> PizzaBuilder {
        pizza.cheese = cheese
        return self
    }
    
    func setSize(size: PizzaSize) -> PizzaBuilder {
        pizza.size = size
        return self
    }
    
    func setAdditions(additions: [String]) -> PizzaBuilder {
        pizza.addditions = additions
        return self
    }
    
    func setCheseSide(included: Bool) -> PizzaBuilder {
        pizza.cheeseSide = included
        return self
    }
    
    func setDoughThickness(_ thickness: DoughThickness) -> PizzaBuilder {
        pizza.dough = thickness
        return self
    }
    
    func build() -> Pizza { pizza }
}

// MARK: - 2 пример.

// Пример из сайта refactoring.guru воплощен на Swift

// Представим, у нас есть класс Дом с кучей параметров:

class House {
    let windows: Int
    let doors: Int
    let rooms: Int
    let numberOfBedrooms: Int
    let numberOfBathrooms: Int
    let squareFootage: Double
    let hasPool: Bool
    let hasGarage: Bool
    let isSmartHome: Bool
    
    var description: String {
        var description = "Это дом с \(numberOfBedrooms) спальнями, \(numberOfBathrooms) ванными комнтами с \(rooms) комнатами в целом и в нем \(windows) окон. Площадь дома: \(squareFootage) кв.м."
        
        if hasPool {
            description += " Также, у него есть бассейн."
        }
        if hasGarage {
            description += " Также, есть собственный гараж на участке дома."
        }
        if isSmartHome {
            description += " И самое замечательное, здесь подключена технология \"Smart House\" (Умный Дом)."
        }
        return description
    }
    
    
    init(windows: Int, doors: Int, rooms: Int, numberOfBedrooms: Int, numberOfBathrooms: Int, squareFootage: Double, hasPool: Bool, hasGarage: Bool, isSmartHome: Bool) {
        self.windows = windows
        self.doors = doors
        self.rooms = rooms
        self.numberOfBedrooms = numberOfBedrooms
        self.numberOfBathrooms = numberOfBathrooms
        self.squareFootage = squareFootage
        self.hasPool = hasPool
        self.hasGarage = hasGarage
        self.isSmartHome = isSmartHome
    }
}

// Большая часть этих параметров будет простаивать, а вызовы конструктора будут выглядеть монструозно из-за длинного списка параметров. К примеру, далеко не каждый дом имеет бассейн, поэтому параметры, связанные с бассейнами, будут простаивать бесполезно в 99% случаев.

// Здесь и применяется паттерн Строитель, чтоб можно было удобно отстроить новый объект класса только с нужными параметрами.

class HouseBuilder {
    private var windows = 0
    private var doors = 0
    private var rooms = 0
    private var numberOfBedrooms = 0
    private var numberOfBathrooms = 0
    private var squareFootage = 0.0
    private var hasPool = false
    private var hasGarage = false
    private var isSmartHome = false
    
    func withWindows(_ windows: Int) -> HouseBuilder {
        self.windows = windows
        return self
    }
    
    func withDoors(_ doors: Int) -> HouseBuilder {
        self.doors = doors
        return self
    }
    
    func withRooms(_ rooms: Int) -> HouseBuilder {
        self.rooms = rooms
        return self
    }
    
    func withNumberOfBedrooms(_ numberOfBedrooms: Int) -> HouseBuilder {
        self.numberOfBedrooms = numberOfBedrooms
        return self
    }
    
    func withNumberOfBathrooms(_ numberOfBathrooms: Int) -> HouseBuilder {
        self.numberOfBathrooms = numberOfBathrooms
        return self
    }
    
    func withSquareFootage(_ squareFootage: Double) -> HouseBuilder {
        self.squareFootage = squareFootage
        return self
    }
    
    func withPool(_ hasPool: Bool) -> HouseBuilder {
        self.hasPool = hasPool
        return self
    }
    
    func withGarage(_ hasGarage: Bool) -> HouseBuilder {
        self.hasGarage = hasGarage
        return self
    }
    
    func withSmartHome(_ isSmartHome: Bool) -> HouseBuilder {
        self.isSmartHome = isSmartHome
        return self
    }
    
    func build() -> House {
        return House(
            windows: windows,
            doors: doors,
            rooms: rooms,
            numberOfBedrooms: numberOfBedrooms,
            numberOfBathrooms: numberOfBathrooms,
            squareFootage: squareFootage,
            hasPool: hasPool,
            hasGarage: hasGarage,
            isSmartHome: isSmartHome
        )
    }
    
}


