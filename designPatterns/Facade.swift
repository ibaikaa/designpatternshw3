//
//  Facade.swift
//  designPatterns
//
//  Created by ibaikaa on 22/3/23.
//

import Foundation

// Фасад – структурный паттерн, позволяющий создать простой интерфейс к сложной системе классов.

// MARK: - 1 пример.
class Engine {
    func start() {
        print("Мотор начал свою работу.")
    }
    
    func stop() {
        print("Мотор заглушился.")
    }
}

class GPS {
    func locate() {
        print("GPS настроен на текущее местоположение.")
    }
    
    func dislocate() {
        print("GPS перестал работать.")
    }
}

// Фасад
class Car {
    let engine = Engine()
    let gps = GPS()
    
    func startCar() {
        engine.start()
        gps.locate()
        print("Машина заработала")
    }
    
    func stopCar() {
        engine.stop()
        gps.dislocate()
        print("Машина перестала работать")
    }
}

// MARK: - 2 пример.

// Пример из реальной жизни: запуск урока в Geeks и окончание урока. Ученик лишь видит готовую к уроку аудиторию, но за подготовкой к уроку и завершением урока стоит множество операций, такие как: включение wifi через модем, включение проектора, подключение hdmi к проектору, подключение проектора к ноутбуку преподавателя через hdmi и для завершения урока все тоже самое, но наоборот. Создадим сложный интерфейс подготовке к уроку/окончания урока, но обращаться будем через удобный интерфейс GeeksLesson с методами startLesson() и stopLesson().

// Сложные интерфейсы

protocol LessonRequirement {
    func actionBeforeLesson()
    func actionAfterLesson()
}

class WifiModem: LessonRequirement {
    func turnOn() {
        print("Модем был включен. Wifi в аудитории работает.")
    }
    
    func turnOff() {
        print("Модем был выключен. Wifi в аудитории не работает.")
    }
    
    func actionBeforeLesson() {
        turnOn()
    }
    
    func actionAfterLesson() {
        turnOff()
    }
   
}

class HDMI: LessonRequirement {
    func connectToProjector() {
        print("HDMI подключен к проектору.")
    }
    
    func disconnectFromProjector() {
        print("HDMI отключен от проектора.")
    }
    
    func actionBeforeLesson() {
        connectToProjector()
    }
    
    func actionAfterLesson() {
        disconnectFromProjector()
    }
    
}

class Projector: LessonRequirement {
    func turnOn() {
        print("Проектор был включен. Картинка работает хорошо.")
    }
    
    func turnOff() {
        print("Проектор был выключен. Пусть остывает.")
    }
    
    func actionBeforeLesson() {
        turnOn()
    }
    
    func actionAfterLesson() {
        turnOff()
    }
    
}

class TeacherLaptop: LessonRequirement {
    func startZoomSession() {
        print("Конференция ЗУМ для урока была создана преподавателем и отправлена в группу в телеграме.")
    }
    
    func stopZoomSession() {
        print("Конференция ЗУМ для урока была окончена.")
    }
    
    func connectToProjector() {
        print("Ноутбук преподавателя был подключен к проектору. Все работает как нужно.")
    }
    
    func disconnectFromProjector() {
        print("Ноутбук преподавателя был отключен от проектора.")
    }
    
    func actionBeforeLesson() {
        connectToProjector()
        startZoomSession()
    }

    func actionAfterLesson() {
        disconnectFromProjector()
        stopZoomSession()
    }
    
}

// Все сложные интерфейсы в одном классе и скрыты от клиента
// Методы startLesson() и stopLesson() позволяют сделать необходимый алгоритм действий
// до и после занятия.

class GeeksLesson {
    private var lessonNeeds: [LessonRequirement] = [
        WifiModem(),
        HDMI(),
        Projector(),
        TeacherLaptop()
    ]
    
    func startLesson() {
        lessonNeeds.forEach { $0.actionBeforeLesson() }
        print("Урок начался.")
    }
    
    func stopLesson() {
        lessonNeeds.forEach { $0.actionAfterLesson() }
        print("Урок окончен.")
    }
    
}



